import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Accueil from './pages/Accueil';
import About from './pages/About';
import NotFound from './pages/NotFound';

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Accueil} />
        <Route path="/a-propos" exact component={About} />
        <Route component={NotFound}/>
      </Switch>
    </BrowserRouter>
  );
};

export default App;






