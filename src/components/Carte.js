import React from 'react';

const Carte = (props) => {
    const catimage = props.catimage;
    console.log(catimage);


    return (
        <li className = "carte">
            <img src={catimage.url} alt="image chat"/>
            <p>{catimage.id}</p>
        </li>
    )
}

export default Carte;