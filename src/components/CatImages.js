import React, {useEffect, useState} from 'react';
import Carte from '../components/Carte';
import axios from 'axios';

const CatImages = ()=>{
    const[data, setData] =useState([]);

    useEffect(() =>{
        axios.get(" https://api.thecatapi.com/v1/images/search?limit=50&page=100&order=DESC", {
            headers: {
                "Content-Type": "application/json",
                "x-api-key": "6c48ad3a-a691-4587-951f-dab7851b8ca7"
            }
        })
    .then((res)=> setData(res.data));

    console.log(data);
    }, []);

    return (
        <div>
            <ul>
               {data.map((catimage) =>(
                   <Carte catimage={catimage} Key={catimage.id}/>
               ))} 
            </ul>
        </div>
    );
    
};
    

export default CatImages;