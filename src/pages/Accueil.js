import React from 'react';
import Navigation from '../components/Navigation';
import CatImages from "../components/CatImages";


const Accueil = ()=>{

    return(
        <div className="accueil">
             <Navigation />
             <h1>Browser of Cats</h1> 
             <CatImages/>
        </div>
    );
};

export default Accueil;