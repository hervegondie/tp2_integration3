import React from 'react';
import Navigation from '../components/Navigation';

const About = () => {
    return (
        <div>
            <Navigation />
            <h1>À propos</h1>
            <br />
            <p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vero possimus ipsum, suscipit soluta repellat quam, alias aliquid recusandae dolores ducimus iure cumque laborum, incidunt praesentium perferendis. Omnis distinctio architecto ut! Quaerat, accusamus. Aliquam dolore provident atque consequuntur saepe et perferendis reiciendis, facilis amet, asperiores, sequi minus. Optio error nesciunt quos deleniti! Delectus id iusto assumenda impedit voluptas distinctio odit itaque.</p>
        </div>
    );
};

export default About;